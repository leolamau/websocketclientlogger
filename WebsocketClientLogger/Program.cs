﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.IO;
using IniParser;
using IniParser.Model;

namespace WebsocketClientLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            var _ws = new WebsocketClient();
            _ws.Connect();
            do
            {

            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            _ws.Disconnect();
        }
    }

    class WebsocketClient
    {
        private WebSocket _ws = null;
        private string _webSocketURL;
        private string _logPath;
        private string _logPrefix;

        #region private
        private bool LoadSettings()
        {
            string iniFile = Path.ChangeExtension(System.AppDomain.CurrentDomain.FriendlyName, ".ini");

            if (File.Exists(iniFile))
            {
                var parser = new FileIniDataParser();
                try
                {
                    IniData data = parser.ReadFile(iniFile);
                    _webSocketURL = data["Settings"]["url"];
                    _logPath = data["Settings"]["log path"];
                    _logPrefix = data["Settings"]["log prefix"];
                    if (_webSocketURL == null)
                    {
                        Console.WriteLine("url setting is blank, please add url=xxxx to Settings section of the ini file.");
                        return false;
                    }
                    else
                    {
                        if (_logPath == null)
                            _logPath = "log";
                        if (_logPrefix == null)
                            _logPrefix = "";
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            else
            {
                Console.WriteLine($"Ini file {iniFile} not found!");
                return false;
            }
        }
        private void WriteToFile(string data)
        {
             
            string filename = $"{AppDomain.CurrentDomain.BaseDirectory}{_logPath}";
            Directory.CreateDirectory(filename);
            filename += $"\\{_logPrefix}{DateTime.UtcNow.ToString("yyyyMMdd")}.txt";
            using (FileStream stream = new FileStream(filename, FileMode.OpenOrCreate|FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(stream))
                {
                    streamWriter.WriteLine(data);
                }
            }

        }
        private void OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsText)
            {
                WriteToFile(e.Data);
            }
            else if (e.IsBinary)
            {
                Console.WriteLine("incoming data is binary");
            }
        }
        #endregion
        #region public
        public WebsocketClient()
        {
            if (LoadSettings())
            {
                _ws = new WebSocket(_webSocketURL);
                _ws.OnOpen += (sender, e) => Console.WriteLine($"Connected to {_webSocketURL}");
                _ws.OnError += (sender, e) => Console.WriteLine($"Error: {e.Message}");
                _ws.OnClose += (sender, e) => Console.WriteLine($"Disconnected {_webSocketURL}");
                _ws.OnMessage += OnMessage;
            }
        }

        public void Connect()
        {
            if (_webSocketURL != null)
            {
                Console.WriteLine($"Connecting to {_webSocketURL}");
                _ws.ConnectAsync();
            }
        }

        public void Disconnect()
        {
            _ws.Close();
        }
        #endregion

    }
}
